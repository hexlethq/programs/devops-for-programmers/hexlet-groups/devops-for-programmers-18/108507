resource "digitalocean_loadbalancer" "loadbalancer" {
  name   = "foofaev-security-loadbalancer"
  region = var.region

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 5000
    target_protocol = "http"

    certificate_name = digitalocean_certificate.cert.name
  }

  healthcheck {
    port     = 5000
    protocol = "tcp"
  }

  vpc_uuid    = digitalocean_vpc.hexlet_vpc.id
  droplet_ids = digitalocean_droplet.servers.*.id
}
