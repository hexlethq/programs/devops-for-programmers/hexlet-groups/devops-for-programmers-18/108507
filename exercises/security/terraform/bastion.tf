resource "digitalocean_droplet" "bastion" {
  image              = "ubuntu-20-04-x64"
  name               = "foofaev-security-bastion"
  region             = var.region
  size               = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys           = [data.digitalocean_ssh_key.foo-wsl2.id]
  vpc_uuid = digitalocean_vpc.hexlet_vpc.id
}
