resource "digitalocean_firewall" "bastion_firewall" {
  name = "bastion-firewall"

  droplet_ids = [digitalocean_droplet.bastion.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol           = "icmp"
    source_droplet_ids = digitalocean_droplet.servers.*.id
  }

  outbound_rule {
    protocol                = "tcp"
    port_range              = "22"
    destination_droplet_ids = digitalocean_droplet.servers.*.id
  }

  outbound_rule {
    protocol                = "icmp"
    destination_droplet_ids = digitalocean_droplet.servers.*.id
  }
}
