resource "digitalocean_droplet" "servers" {
  count              = 2
  image              = "docker-20-04"
  name               = "foofaev-security-${count.index + 1}"
  region             = var.region
  size               = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys           = [data.digitalocean_ssh_key.foo-wsl2.id]
  vpc_uuid = digitalocean_vpc.hexlet_vpc.id
}
