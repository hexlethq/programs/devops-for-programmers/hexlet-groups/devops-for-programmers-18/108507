variable "do_token" {
  description = "DigitalOcean Personal Access Token"
  type        = string
}

variable "pvt_key" {
  description = "Private ssh key location, so Terraform can use it to log in to new Droplets"
  type        = string
  default     = "~/.ssh/id_rsa"
}

variable "region" {
  description = "Services region"
  type        = string
  default     = "fra1"
}
