output "webservers" {
  value = [for droplet in digitalocean_droplet.servers : {
    name = droplet.name
    ipv4_address_private = droplet.ipv4_address_private
  }]
}
output "bastion_ip" {
  value = digitalocean_droplet.bastion.ipv4_address
}
