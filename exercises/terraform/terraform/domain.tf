resource "digitalocean_domain" "my_domain" {
  name       = "weryha.xyz"
  ip_address = digitalocean_loadbalancer.web-lb.ip
}
