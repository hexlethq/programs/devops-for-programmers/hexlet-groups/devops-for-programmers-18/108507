resource "digitalocean_droplet" "web-1" {
  image    = "docker-20-04"
  name     = "foofaev-web-terraform-homework-01"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.foo-wsl2.id]
}

