resource "digitalocean_droplet" "web-2" {
  image    = "docker-20-04"
  name     = "foofaev-web-terraform-homework-02"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.foo-wsl2.id]
}

