terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  description = "DigitalOcean Personal Access Token"
  type        = string
}

variable "pvt_key" {
  description = "Private key location, so Terraform can use it to log in to new Droplets"
  type        = string
}

provider "digitalocean" {
  token = var.do_token
}
