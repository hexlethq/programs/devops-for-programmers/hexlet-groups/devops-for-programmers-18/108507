resource "digitalocean_domain" "default" {
  name       = "weryha.xyz"
  ip_address = digitalocean_loadbalancer.loadbalancer.ip
}
