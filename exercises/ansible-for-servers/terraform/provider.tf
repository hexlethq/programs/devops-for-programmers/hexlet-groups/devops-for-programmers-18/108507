terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "do_token" {
  description = "DigitalOcean Personal Access Token"
  type        = string
}

variable "pvt_key" {
  description = "Private ssh key location, so Terraform can use it to log in to new Droplets"
  type        = string
  default     = "~/.ssh/id_rsa"
}

provider "digitalocean" {
  token = var.do_token
}
