resource "digitalocean_droplet" "servers" {
  count              = 2
  image              = "ubuntu-20-10-x64"
  name               = "foofaev-ansible-for-servers-${count.index + 1}"
  region             = "ams3"
  size               = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys           = [data.digitalocean_ssh_key.foo-wsl2.id]
}
