resource "digitalocean_loadbalancer" "loadbalancer" {
  name   = "foofaev-ansible-for-servers-loadbalancer"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_ids = digitalocean_droplet.servers.*.id
}
