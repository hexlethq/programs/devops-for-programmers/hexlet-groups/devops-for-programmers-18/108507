resource "digitalocean_droplet" "servers" {
  count              = 2
  image              = "ubuntu-21-04-x64"
  name               = "service-discovery-${count.index + 1}"
  region             = "ams3"
  size               = "s-1vcpu-1gb"
  private_networking = true

  // BEGIN
  ssh_keys = [data.digitalocean_ssh_key.foo-wsl2.id]
  // END
}
