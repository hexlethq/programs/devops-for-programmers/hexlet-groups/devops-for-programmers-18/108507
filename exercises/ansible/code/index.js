import Fastify from 'fastify';

const fastify = Fastify({ logger: true });

fastify.get('/', (request, reply) => {
  reply.send({ hello: 'world' });
});

fastify.listen(process.env.PORT || 3000, (error, address) => {
  if (error) {
    fastify.log.error(error);
    process.exit(1);
  }
  fastify.log.info(`server listening on ${address}`);
});
