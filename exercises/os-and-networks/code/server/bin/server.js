#! /usr/bin/env node

import getApp from '..';

const port = process.env.PORT || 5000;

getApp().listen(process.env.PORT || 3000, (error, address) => {
  if (error) {
    console.error(error);
    process.exit(1);
  }
  console.log(`Server is running on address: ${address}`); // eslint-disable-line no-console
});
